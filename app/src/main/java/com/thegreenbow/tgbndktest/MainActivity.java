package com.thegreenbow.tgbndktest;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.thegreenbow.tgbndktest.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    // Used to load the 'tgbndktest' library on application startup.
    static {
        System.loadLibrary("tgbndktest");
    }

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Example of a call to a native method
        TextView tv = binding.sampleText;

        Bag myBag = new Bag();
        //tv.setText(addFizzBuzz(myBag , 23));
        addFizzBuzz(myBag , 23) ;

        tv.setText(myBag.getFirst());
    }

    /**
     * A native method that is implemented by the 'tgbndktest' native library,
     * which is packaged with this application.
     */

    
    public native String getFizzBuzz (int count) ;



    public native void  addFizzBuzz (Bag bag , int count) ;
}